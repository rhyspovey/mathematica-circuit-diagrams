(* ::Package:: *)

(* ::Title:: *)
(*Circuit Diagrams*)


(* ::Text:: *)
(*Rhys Povey <www.rhyspovey.com>*)


(* ::Text:: *)
(*Option "Width" by default is Automatic based on AspectRatio, setting it will override.*)
(*Most objects have VertexColors or Background option.*)


(* ::Text:: *)
(*PDF export does not colour gradients on lines, use Graphics[]/.Line[\[FormalX]__]:>SubdivideLine[Line[\[FormalX]]] to split them up.*)


(* ::Section:: *)
(*Front End*)


BeginPackage["CircuitDiagrams`"];


(* ::Subsection:: *)
(*Objects*)


Resistor::usage="Resistor[start,end] has options AspectRatio, \"Width\", \"Periods\", \"Flange\".";


Capacitor::usage="Capacitor[start,end] has options AspectRatio, \"Width\".";


Inductor::usage="Inductor[start,end] has options AspectRatio, \"Width\", \"Periods\", \"Flange\".";


Impedance::usage="Impedance[start,end] has options AspectRatio, \"Width\".";


Drive::usage="Drive[start,end] for a signal generator.";


SquareDrive::usage="SquareDrive[start,end] for a digital signal generator.";


Amplifier::usage="Amplifier[start,end] has options \"Width\", Reverse, \"Text\".";


LockInAmplifier::usage="LockInAmplifier[start,end] has options \"Width\", Reverse.";


Circulator::usage="Circulator[start,end,direction] takes direction 1 (anticlockwise) or -1 (clockwise). Has options \"ArrowScaling\", \"ArrowAngle\", \"StartAngle\", \"Arc\".";


Terminator::usage="Terminator[start,end,] has options AspectRatio, \"Width\".";


Ground::usage="Ground[start,end] has options \"Width\", \"Periods\", Reverse.";


VoltageSource::usage="VoltageSource[start,end] has options \"SymbolScaling\", \"LocationScaling\", Reverse.";


CurrentSource::usage="CurrentSource[start,end] has options \"ArrowScaling\", \"ArrowAngle\", Reverse.";


CurrentSourceAlt::usage="CurrentSourceAlt[start,end] has options Reverse.";


CircuitMeter::usage="CircuitMeter[start,end,letter] has options FontFamily, \"FontScaling\".";


Mixer::usage="Mixer[start,end] has options \"Anlge\", \"Lines\".";


TransmissionLine::usage="TransmissionLine[start,end] uses Bezier curves. Has options AspectRatio, \"Width\".";


TransmissionLineAlt::usage="TransmissionLineAlt[start,end] uses circles and supports VertexColors. Has options AspectRatio, \"Width\".";


Splitter::usage="Splitter[start,end] has options AspectRatio, \"Width\", \"Margin\".";


HybridCoupler::usage="HybridCoupler[start,end] has options AspectRatio, \"Width\", \"Margin\".";


DirectionalCoupler::usage="DirectionalCoupler[start,end,direction] can have direction = 1,-1. Has options AspectRatio, \"Width\", \"Margin\".";


DirectionalCouplerCentered::usage="DirectionalCoupler[start,end,direction] can have direction = 1,-1. Has options AspectRatio, \"Width\", \"Margin\".";


DirectionalCoupler::VertexColors="VertexColors currently not implemented on DirectionalCoupler.";


Isolator::usage="Isolator[start,end] has options \"ArrowScaling\", \"ArrowAngle\", AspectRatio, \"Width\".";


CircuitBox::usage="CircuitBox[start,end,letter] has options FontFamily, \"FontScaling\", AspectRatio, \"Width\".";


BandPassFilter::usage="BandPassFilter[start,end] has options AspectRatio, \"Width\".";


DCBlock::usage="DCBlock[start,end] has options AspectRatio, \"Width\".";


SpectrumAnalyzerCircle::usage="SpectrumAnalyzerCircle[start,end] for a circle spectrum analyzer, has options \"Frequency\".";


SpectrumAnalyzerBlock::usage="SpectrumAnalyzerBlock[start,end] for a rectangular spectrum analyzer, has options \"Frequency\", AspectRatio, \"Width\".";


Oscilloscope::usage="Oscilloscope[start,end] for an oscilloscope."


JosephsonJunction::usage="JosephsonJunction[start,end] for a Josephson junction."


Squid::usage="Squid[start,end] has options AspectRatio, \"Width\", \"Ratio\".";


NoiseDrive::usage="NoiseDrive[start,end] for a voltage noise source.";


(* ::Subsection::Closed:: *)
(*Optics*)


Laser::usage="Laser[start,end] has options AspectRatio, \"Width\", \"LaserWidth\".";


MZM::usage="MZM[start,end]";


PolarizerBox::usage="PolarizerBox[start,end] has option \"PolarizerAngle\", \"Lines\".";


PhotoDetector::usage="PhotoDetector[start,end]";


CirculatorBox::usage="CirculatorBox[start,end,dir] takes dir \[Element] {1,-1}.";


AmplifierBox::usage="AmplifierBox[start,end]";


Stripper::usage="Stripper[start,end] goes from jacketed to bare for a pair.";


VGroove::usage="VGroove[start,end] for a pair.";


FiberConnector::usage="FiberConnector[position,angle] for horizontal connections.";


FiberBox::usage="FiberBox[start,end] for box with connector ends.";


FiberLine::usage="FiberLine[start,end,start cut angle,end cut angle]";


FiberHalfBend::usage="FiberHalfBend[start, end, start cut angle, end cut angle, start direction]";


FiberHalfBend::dir="Direction `1` is not perpindicular to points vector `2`.";


FiberS::usage="FiberS[start, end, start cut angle, end cut angle, direction].";


(* ::Subsection::Closed:: *)
(*Markers*)


CurrentArrow::usage="CurrentArrow[location,direction,size] takes direction as a vector.";


VoltageArrow::usage="VoltageArrow[location,direction,size] takes direction 1 (anticlockwise) or -1 (clockwise). Has options \"ArrowScaling\", \"ArrowAngle\", \"StartAngle\", \"Arc\".";


(* ::Subsection::Closed:: *)
(*Compositions*)


Circuit::usage="Circuit[{\!\(\*SubscriptBox[\(obj\), \(1\)]\),\!\(\*SubscriptBox[\(obj\), \(2\)]\),\[Ellipsis]}] automatically adds wires between objects.";


Circuit::connection="Connection type `1` not recognized.";


(* ::Subsection::Closed:: *)
(*Tools*)


SubdivideLine::usage="SubdivideLine[Line[list]] will subdivide a line if it has VertexColors option.";


SubdivideLine::opts="Can't have 0 minimum length and \[Infinity] maximum segments.";


SubdivideLine::small="Line length smaller than minimum segment length.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


TextToCurve[text_]:=First[First@ImportString[ExportString[text,"PDF"],"PageGraphics","TextMode"->"Outlines"]][[2,1]]


(* ::Subsection::Closed:: *)
(*Bezier code*)


CubicBezierFunction[\[FormalX]_List/;Length[\[FormalX]]==4][\[FormalT]_]:=(1-\[FormalT])^3 \[FormalX][[1]] + 3(1-\[FormalT])^2 \[FormalT] \[FormalX][[2]]+3(1-\[FormalT]) \[FormalT]^2 \[FormalX][[3]]+\[FormalT]^3 \[FormalX][[4]];


QuarterCircleBezierPoints[center_List,radius_:1,startangle_:0,direction_:1]:=Module[{c=4/3 (Sqrt[2]-1),d=Sign[direction]},
(radius RotationMatrix[startangle] . #+center)&/@{{1,0},{1,c d},{c,d},{0,d}}
];


AcuteCircleBezierPoints[center_List,radius_:1,startangle_:0,spanangle_:\[Pi]/4]:=Module[{min,c},
min=Minimize[Sign[spanangle] Integrate[(1-Evaluate[(#[[1]]^2+#[[2]]^2)&@CubicBezierFunction[{{1,0},{1,c},{Cos[spanangle],Sin[spanangle]}+c{Sin[spanangle],-Cos[spanangle]},{Cos[spanangle],Sin[spanangle]}}][\[Phi]/spanangle]])^2,{\[Phi],0,spanangle}],c];
(radius RotationMatrix[startangle] . #+center)&/@{{1,0},{1,c},{Cos[spanangle],Sin[spanangle]}+c{Sin[spanangle],-Cos[spanangle]},{Cos[spanangle],Sin[spanangle]}}/.min[[2]]
]/;If[Abs[spanangle]<=\[Pi]/2,True,Message[AcuteCircleBezierPoints::span,spanangle];False];


(* ::Subsection:: *)
(*Objects*)


$CircuitObjects={
Resistor,
Capacitor,
Inductor,
Drive,
SquareDrive,
Amplifier,
LockInAmplifier,
Impedance,
Circulator,
Terminator,
Ground,
VoltageSource,
CurrentSource,
CurrentSourceAlt,
CircuitMeter,
Mixer,
TransmissionLine,
TransmissionLineAlt,
Splitter,
HybridCoupler,
DirectionalCoupler,
DirectionalCouplerCentered,
Isolator,
CircuitBox,
BandPassFilter,
DCBlock,
SpectrumAnalyzerCircle,
SpectrumAnalyzerBlock,
Oscilloscope,
JosephsonJunction,
Squid,
NoiseDrive
};


Resistor[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,periods,flange,vertexcolors},
vector=p2-p1;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
periods=OptionValue["Periods"];
flange=length OptionValue["Flange"];
vertexcolors=If[OptionValue[VertexColors]=!=None,
Join[
If[flange>0,{First@OptionValue[VertexColors]},{}],
If[Length[OptionValue[VertexColors]]>1,
Blend[OptionValue[VertexColors],#]&/@Subdivide[2 periods+1],
ConstantArray[First@OptionValue[VertexColors],2periods+2]
],If[flange>0,{Last@OptionValue[VertexColors]},{}]
],
None];
Line[
Join[{If[flange>0,p1-normvec flange,Nothing],p1},
Flatten[Table[{p1+(\[FormalI]-1+1/4)/periods vector+orth width/2,p1+(\[FormalI]-1+3/4)/periods vector-orth width/2},{\[FormalI],periods}],1],
{p2,If[flange>0,p2+normvec flange,Nothing]}],VertexColors->vertexcolors
]
];


Options[Resistor]={AspectRatio->4,"Periods"->5,"Width"->Automatic,"Flange"->0.1,VertexColors->None};


Capacitor[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,angle,length,width,size,docolors},
vector=p2-p1;
angle=ArcTan@@vector;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],size OptionValue[AspectRatio]];
size=length/2;
docolors=ListQ[OptionValue[VertexColors]]\[And](Length[OptionValue[VertexColors]]==2);
Sequence@@{
{If[docolors,OptionValue[VertexColors][[1]],Nothing],Translate[Rotate[Rectangle[{-size/2,-width/2},{size/2,width/2}],angle,{0,0}],p1]},
{If[docolors,OptionValue[VertexColors][[2]],Nothing],Translate[Rotate[Rectangle[{-size/2,-width/2},{size/2,width/2}],angle,{0,0}],p2]}
}
];


Options[Capacitor]={AspectRatio->6,"Width"->Automatic,VertexColors->None};


Inductor[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,angle,length,width,flange,periods,s,x,points,vertexcolors},
vector=p2-p1;
normvec=Normalize[vector];
angle=ArcTan@@vector;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
periods=OptionValue["Periods"];
flange=length OptionValue["Flange"];
x=1/2 length/(periods+1);
s=(-2x+length)/((1+2 periods) \[Pi]);
points=FirstCase[ParametricPlot[{x Cos[\[FormalQ]]+(\[FormalQ]+\[Pi])s+x,width/2 Sin[\[FormalQ]]},{\[FormalQ],-\[Pi],periods 2\[Pi]},AspectRatio->Automatic,PlotRange->{{0,length},Automatic},Axes->False,ImageMargins->0],Line[\[FormalX]__]:>\[FormalX],Missing[],\[Infinity]];
vertexcolors=If[OptionValue[VertexColors]=!=None,
Join[
If[flange>0,{First@OptionValue[VertexColors]},{}],
If[Length[OptionValue[VertexColors]]>1,
Blend[OptionValue[VertexColors],#]&/@Subdivide[Length[points]-1],
ConstantArray[First@OptionValue[VertexColors],Length[points]]
],
If[flange>0,{Last@OptionValue[VertexColors]},{}]
],
None];
Translate[Rotate[Line[Join[If[flange>0,{{-flange,0}},{}],points,If[flange>0,{{length+flange,0}},{}]],VertexColors->vertexcolors],angle,{0,0}],p1]
];


Options[Inductor]={AspectRatio->3,"Periods"->4,"Width"->Automatic,"Flange"->0.1,VertexColors->None};


Impedance[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True]
}
];


Options[Impedance]={AspectRatio->2,"Width"->Automatic,Background->None};


Drive[p1_List,p2_List,OptionsPattern[]]:=Module[{c,r,sr},
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
sr=0.75;
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
FirstCase[Plot[sr/2 r Sin[(2\[Pi])/(2sr r) (\[FormalX]-c[[1]])]+c[[2]],{\[FormalX],c[[1]]-sr r,c[[1]]+sr r}],Line[__],Missing[],\[Infinity]]}
];


Options[Drive]={Background->None};


SquareDrive[p1_List,p2_List,OptionsPattern[]]:=Module[{c,r,s,h},
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
s=0.75r;
h=0.5r;
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
Line[(c+#)&/@{{-s,0},{-s/2,0},{-s/2,-h},{0,-h},{0,h},{s/2,h},{s/2,0},{s,0}}]}
];


Options[SquareDrive]={Background->None};


Amplifier[in1_List,in2_List,OptionsPattern[]]:=Module[{p1,p2,vector,orth,length,width,pts,c,textcurve,textc},
{p1,p2}=If[OptionValue[Reverse],Reverse[{in1,in2}],{in1,in2}];
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],2 length/Sqrt[3]];
c=(2p1/3+p2/3);
pts={p1+orth width/2,p2,p1-orth width/2};

If[StringQ[OptionValue["Text"]],
textcurve=TextToCurve[Style[OptionValue["Text"],FontFamily->OptionValue[FontFamily],FontSize-> width OptionValue["FontScaling"]]];
textc=Table[Mean@MinMax@Flatten[textcurve/.{FilledCurve[_,\[FormalX]_]:>\[FormalX][[All,All,\[FormalI]]]}],{\[FormalI],2}];
];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
If[StringQ[OptionValue["Text"]],
	If[OptionValue[FontColor]=!=Automatic,{OptionValue[FontColor],Translate[textcurve,c-textc]},Translate[textcurve,c-textc]],
	Nothing
]
}
];


Options[Amplifier]={"Width"->Automatic,Background->None,Reverse->False,"Text"->None,"FontScaling"->1,FontFamily->"Calibri",FontColor->Automatic};


LockInAmplifier[in1_List,in2_List,OptionsPattern[]]:=Module[{p1,p2,vector,orth,length,width,pts,c,r,\[Phi]},
{p1,p2}=If[OptionValue[Reverse],Reverse[{in1,in2}],{in1,in2}];
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],2 length/Sqrt[3]];
c=(2p1/3+p2/3);
\[Phi]=ArcTan[(width/2)/length];
r=Sin[\[Phi]]/Sin[3\[Pi]/4-\[Phi]] 2 length/3;
pts={p1+orth width/2,p2,p1-orth width/2};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{c-vector/3+orth width/3,c+(Normalize@vector-orth)r/Sqrt[2]}],
Line[{c-vector/3-orth width/3,c+(Normalize@vector+orth)r/Sqrt[2]}]
}
];


Options[LockInAmplifier]={"Width"->Automatic,Background->None,Reverse->False};


Circulator[p1_List,p2_List,dir_Integer:1,OptionsPattern[]]:=Module[{c,r,\[Theta],l,\[Phi]arc,\[Phi]start,\[Phi]end,t},
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
t=1/2;
l=r OptionValue["ArrowScaling"];
\[Theta]=OptionValue["ArrowAngle"];
\[Phi]arc=OptionValue["Arc"];
\[Phi]start=Switch[OptionValue["StartAngle"],
	Automatic,ArcTan@@(p1-p2),
	"Fixed",-\[Pi] +((dir-1)/2) \[Pi]/4,
	_?(NumberQ@N@#&),OptionValue["StartAngle"]
];
\[Phi]end=\[Phi]start+dir \[Phi]arc;
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
Circle[c,r/2,{\[Phi]start,\[Phi]end}],
Line[{
c+{t r Cos[\[Phi]end],t r Sin[\[Phi]end]}+RotationMatrix[\[Phi]end-dir 2\[Theta]] . {0,-l dir},
c+{t r Cos[\[Phi]end],t r Sin[\[Phi]end]},
c+{t r Cos[\[Phi]end],t r Sin[\[Phi]end]}+RotationMatrix[\[Phi]end+dir \[Theta]] . {0,-l dir}
}]}
];


Options[Circulator]={"ArrowScaling"->0.25,"ArrowAngle"->\[Pi]/8,"StartAngle"->"Fixed","Arc"->5\[Pi]/4,Background->None};


Terminator[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[pts[[{1,3}]]]
}
];


Options[Terminator]={AspectRatio->1,"Width"->Automatic,Background->None};


Ground[in1_List,in2_List,OptionsPattern[]]:=Module[{p1,p2,vector,orth,length,width,periods,vertexcolors,lines},
{p1,p2}=If[OptionValue[Reverse],Reverse[{in1,in2}],{in1,in2}];
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],2 length/Sqrt[3]];
periods=OptionValue["Periods"];
vertexcolors=If[OptionValue[VertexColors]=!=None,
Switch[Length[OptionValue[VertexColors]],
1,ConstantArray[First@OptionValue[VertexColors],periods],
periods,OptionValue[VertexColors],
_,Blend[OptionValue[VertexColors],#]&/@Subdivide[periods-1]
],
None];

lines=Table[
Line[{p1+vector (\[FormalI]-1)/periods-orth width/2 (1-(\[FormalI]-1)/periods),p1+vector (\[FormalI]-1)/periods+orth width/2 (1-(\[FormalI]-1)/periods)
}],{\[FormalI],periods}];

Sequence@@If[ListQ@vertexcolors,Join[{vertexcolors}\[Transpose],{lines}\[Transpose],2],lines]

];


Options[Ground]={"Width"->Automatic,"Periods"->3,VertexColors->None,Reverse->False};


VoltageSource[in1_List,in2_List,OptionsPattern[]]:=Module[{p1,p2,vector,normvec,c,r,l,s},
{p1,p2}=If[OptionValue[Reverse],Reverse[{in1,in2}],{in1,in2}];
vector=p2-p1;
normvec=Normalize[vector];
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
l=r OptionValue["SymbolScaling"];
s=r OptionValue["LocationScaling"];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
Line[{c-s normvec-{l,0},c-s normvec+{l,0}}],
Line[{c+s normvec-{l,0},c+s normvec+{l,0}}],Line[{c+s normvec-{0,l},c+s normvec+{0,l}}]}
];


Options[VoltageSource]={"SymbolScaling"->0.2,"LocationScaling"->0.5,Reverse->False,Background->None};


CurrentSource[in1_List,in2_List,OptionsPattern[]]:=Module[{p1,p2,vector,c,r,l,\[Theta],t},
{p1,p2}=If[OptionValue[Reverse],Reverse[{in1,in2}],{in1,in2}];
vector=p2-p1;
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
l=r OptionValue["ArrowScaling"];
\[Theta]=OptionValue["ArrowAngle"];
t=3/5;

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
Line[{c-t vector/2,c+t vector/2}],
Line[{c+t vector/2+RotationMatrix[\[Theta]] . (-l vector),c+t vector/2,c+t vector/2+RotationMatrix[-\[Theta]] . (-l vector)}]}
];


Options[CurrentSource]={"ArrowScaling"->0.25,"ArrowAngle"->\[Pi]/6,Reverse->False,Background->None};


CurrentSourceAlt[in1_List,in2_List,OptionsPattern[]]:=Module[{p1,p2,vector,r},
{p1,p2}=If[OptionValue[Reverse],Reverse[{in1,in2}],{in1,in2}];
vector=p2-p1;
r=Norm[p2-p1]/3;
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[p1+vector/3,r],Disk[p1+2vector/3,r]},Nothing],
Circle[p1+vector/3,r],Circle[p1+2vector/3,r]
}
];


Options[CurrentSourceAlt]={Reverse->False,Background->None};


CircuitMeter[p1_List,p2_List,letter_String,OptionsPattern[]]:=Module[{vector,c,r,textcurve,textc},
vector=p2-p1;
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
textcurve=TextToCurve[Style[letter,FontFamily->OptionValue[FontFamily],FontSize-> 2 r OptionValue["FontScaling"]]];
textc=Table[Mean@MinMax@Flatten[textcurve/.{FilledCurve[_,\[FormalX]_]:>\[FormalX][[All,All,\[FormalI]]]}],{\[FormalI],2}];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
If[OptionValue[FontColor]=!=Automatic,{OptionValue[FontColor],Translate[textcurve,c-textc]},Translate[textcurve,c-textc]]
}
];


Options[CircuitMeter]={"FontScaling"->1,FontFamily->"Calibri",Background->None,FontColor->Automatic};


Mixer[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,c,r,lines,\[Phi]start},
vector=p2-p1;
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
lines=OptionValue["Lines"];
\[Phi]start=If[NumberQ@N@OptionValue["Angle"],
OptionValue["Angle"],
ArcTan@@vector+\[Pi]/4
];
Sequence@@Join[
{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r]},
Table[Line[{c+r{Cos[\[Phi]],Sin[\[Phi]]},c-r{Cos[\[Phi]],Sin[\[Phi]]}}],{\[Phi],\[Phi]start,\[Phi]start+\[Pi]-\[Pi]/lines,\[Pi]/lines}]
]
];


Options[Mixer]={"Angle"->Automatic,"Lines"->2,Background->None};


TransmissionLine[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,end,b},
vector=p2-p1;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
end=OptionValue["EndScaling"]width;
b=4/3 Tan[\[Pi]/(2 4)]; (* 4 point Bezier circle approx constant *)
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],
FilledCurve[{
Line[{p1+orth width/2,p2+orth width/2}],
BezierCurve[{p2+orth width/2+normvec end b,p2-orth width/2+normvec end b,p2-orth width/2}],
Line[{p1-orth width/2}],
BezierCurve[{p1-orth width/2-normvec end b,p1+orth width/2-normvec end b,p1+orth width/2}]
}]},Nothing],
JoinedCurve[{
Line[{p1+orth width/2,p2+orth width/2}],
BezierCurve[{p2+orth width/2+normvec end b,p2-orth width/2+normvec end b,p2-orth width/2}],
Line[{p1-orth width/2}],
BezierCurve[{p1-orth width/2-normvec end b,p1+orth width/2-normvec end b,p1+orth width/2}]
}
,CurveClosed->True],
BezierCurve[{p2+orth width/2,p2+orth width/2-normvec end b,p2-orth width/2-normvec end b,p2-orth width/2}]
}
];


Options[TransmissionLine]={AspectRatio->4,"EndScaling"->1/2,"Width"->Automatic,Background->None,VertexColors->None};


TransmissionLineAlt[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,\[Phi],normvec,orth,length,width,end,vertexcolors},
vector=p2-p1;
\[Phi]=ArcTan@@vector;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
end=OptionValue["EndScaling"]width/2;
vertexcolors=OptionValue[VertexColors];
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],
Polygon[{p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2}],
Rotate[Disk[p1,{end,width/2},{\[Pi]/2,3\[Pi]/2}],\[Phi],p1],
Rotate[Disk[p2,{end,width/2}],\[Phi],p2]
},Nothing],
Line[{p1+orth width/2,p2+orth width/2},VertexColors->vertexcolors],
Line[{p1-orth width/2,p2-orth width/2},VertexColors->vertexcolors],
{If[ListQ[vertexcolors],First@vertexcolors,Nothing],Rotate[Circle[p1,{end,width/2},{\[Pi]/2,3\[Pi]/2}],\[Phi],p1]},
{If[ListQ[vertexcolors],Last@vertexcolors,Nothing],Rotate[Circle[p2,{end,width/2}],\[Phi],p2]}
}
];


Options[TransmissionLineAlt]={AspectRatio->4,"EndScaling"->1/2,"Width"->Automatic,Background->None,VertexColors->None};


Splitter[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,y,margin,pts,center,vertexcolors,blend},
vector=p2-p1;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
y=width/(2 2);
margin=If[OptionValue["Margin"]===Automatic,0.1 width,OptionValue["Margin"]];
blend=(y/Sin[\[Pi]/3])/(y/Sin[\[Pi]/3]+length/2-y Tan[\[Pi]/3]);
vertexcolors=If[OptionValue[VertexColors]=!=None,
{{#[[1]],#[[1]]},{#[[1]],Blend[{#[[1]],#[[2]]},blend],#[[2]]},{#[[1]],Blend[{#[[1]],#[[3]]},blend],#[[3]]}}&@
If[ListQ[OptionValue[VertexColors]],Switch[Length[OptionValue[VertexColors]],
3,OptionValue[VertexColors],
2,Append[OptionValue[VertexColors],Last[OptionValue[VertexColors]]],
1,ConstantArray[First[OptionValue[VertexColors]],3]
],
ConstantArray[OptionValue[VertexColors],3]
],
ConstantArray[None,3]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
center=Mean[pts];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{p1+normvec margin,center},VertexColors->vertexcolors[[1]]],
Line[{center,center+y/Tan[\[Pi]/3] normvec+y orth,p2+y orth-normvec margin},VertexColors->vertexcolors[[2]]],
Line[{center,center+y/Tan[\[Pi]/3] normvec-y orth,p2-y orth-normvec margin},VertexColors->vertexcolors[[3]]]
}
];


Options[Splitter]={AspectRatio->1,"Width"->Automatic,"Margin"->Automatic,VertexColors->None,Background->None};


HybridCoupler[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,x,y,margin,pts,center,vertexcolors,blend},
vector=p2-p1;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
x=length/(2 2);
y=width/(2 2);
margin=If[OptionValue["Margin"]===Automatic,0.1 width,OptionValue["Margin"]];
blend=(y/Sin[\[Pi]/3])/(y/Sin[\[Pi]/3]+y/2-y Tan[\[Pi]/3]);
vertexcolors=If[OptionValue[VertexColors]=!=None,
{{#[[1]],#[[1]],#[[2]],#[[2]]},{#[[3]],#[[3]],#[[4]],#[[4]]},{#[[1]],#[[3]]},{#[[2]],#[[4]]}}&@
If[ListQ[OptionValue[VertexColors]],Switch[Length[OptionValue[VertexColors]],
4,OptionValue[VertexColors],
3,Append[OptionValue[VertexColors],Last[OptionValue[VertexColors]]],
2,Join[OptionValue[VertexColors],Reverse[OptionValue[VertexColors]]],
1,ConstantArray[First[OptionValue[VertexColors]],4]
],
ConstantArray[OptionValue[VertexColors],4]
],
ConstantArray[None,4]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
center=Mean[pts];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{p1+y orth+normvec margin,center-x normvec+y orth,center+x normvec+y orth,p2+y orth-normvec margin},VertexColors->vertexcolors[[1]]],
Line[{p2-y orth-normvec margin,center+x normvec-y orth,center-x normvec-y orth,p1-y orth+normvec margin},VertexColors->vertexcolors[[2]]],
Line[{center-x normvec+y orth,center+x normvec-y orth},VertexColors->vertexcolors[[3]]],
Line[{center+x normvec+y orth,center-x normvec-y orth},VertexColors->vertexcolors[[4]]]
}
];


Options[HybridCoupler]={AspectRatio->2,"Width"->Automatic,"Margin"->Automatic,VertexColors->None,Background->None};


DirectionalCoupler[p1_List,p2_List,dir:-1|1:1,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,x,y,margin,pts,center,vertexcolors,blend},
vector=p2-p1;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
x=length/(2 2);
y=width/(2 2);
margin=If[OptionValue["Margin"]===Automatic,0.1 width,OptionValue["Margin"]];
blend=(y/Sin[\[Pi]/3])/(y/Sin[\[Pi]/3]+y/2-y Tan[\[Pi]/3]);
vertexcolors=If[OptionValue[VertexColors]=!=None,Message[DirectionalCoupler::VertexColors]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
center=Mean[pts];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{p1-y dir orth+normvec margin,center-x normvec-y dir orth,center+x normvec-y dir orth,p2-y dir orth-normvec margin}],
Rotate[Circle[center-x normvec+y dir orth,{2x,2y},{-dir \[Pi]/2,0}],ArcTan@@normvec,center-x normvec+y dir orth],
Line[{center+x normvec+y dir orth,center+x normvec+(width/2-margin) dir orth}]
}
];


Options[DirectionalCoupler]={AspectRatio->1,"Width"->Automatic,"Margin"->Automatic,VertexColors->None,Background->None};


DirectionalCouplerCentered[p1_List,p2_List,dir:-1|1:1,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,x,y,margin,pts,center,vertexcolors,blend},
vector=p2-p1;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
x=length/(2 2);
y=width/(2 2);
margin=If[OptionValue["Margin"]===Automatic,0.1 width,OptionValue["Margin"]];
blend=(y/Sin[\[Pi]/3])/(y/Sin[\[Pi]/3]+y/2-y Tan[\[Pi]/3]);
vertexcolors=If[OptionValue[VertexColors]=!=None,Message[DirectionalCoupler::VertexColors]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
center=Mean[pts];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{p1+normvec margin,center-x normvec,center+x normvec,p2-normvec margin}],
Rotate[Circle[center-x normvec+y dir orth,{x,y},{-dir \[Pi]/2,0}],ArcTan@@normvec,center-x normvec+y dir orth],
Line[{center+y dir orth,center+(width/2-margin) dir orth}]
}
];


Options[DirectionalCouplerCentered]={AspectRatio->1,"Width"->Automatic,"Margin"->Automatic,VertexColors->None,Background->None};


Isolator[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts,c,l,\[Theta],t},
vector=p2-p1;
c=(p1+p2)/2;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
l=width OptionValue["ArrowScaling"];
\[Theta]=OptionValue["ArrowAngle"];
t=3/5;

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{c-t vector/2,c+t vector/2}],
Line[{c+t vector/2+RotationMatrix[\[Theta]] . (-l vector),c+t vector/2,c+t vector/2+RotationMatrix[-\[Theta]] . (-l vector)}]}
];


Options[Isolator]={"ArrowScaling"->0.25,"ArrowAngle"->\[Pi]/6,AspectRatio->2,"Width"->Automatic,Background->None};


CircuitBox[p1_List,p2_List,letter_String,OptionsPattern[]]:=Module[{vector,c,orth,length,width,pts,textcurve,textc,s},
vector=p2-p1;
c=(p1+p2)/2;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
s=Min[length,width];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
textcurve=TextToCurve[Style[letter,FontFamily->OptionValue[FontFamily],FontSize-> s OptionValue["FontScaling"]]];
textc=Table[Mean@MinMax@Flatten[textcurve/.{FilledCurve[_,\[FormalX]_]:>\[FormalX][[All,All,\[FormalI]]]}],{\[FormalI],2}];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
If[OptionValue[FontColor]=!=Automatic,{OptionValue[FontColor],Translate[textcurve,c-textc]},Translate[textcurve,c-textc]]
}
];


Options[CircuitBox]={"FontScaling"->1,FontFamily->"Calibri",Background->None,FontColor->Automatic,AspectRatio->1,"Width"->Automatic};


BandPassFilter[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,c,orth,length,width,pts,center,s,w,pts2},
vector=p2-p1;
normvec=Normalize[vector];
c=(p1+p2)/2;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
s=Min[length,width];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
pts2=(c+s #)&/@{{-2/6,-1/4},{-1/6,1/4},{1/6,1/4},{2/6,-1/4}};

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
JoinedCurve[Line[pts2],CurveClosed->False]
}
];


Options[BandPassFilter]={AspectRatio->1,"Width"->Automatic,Background->None};


DCBlock[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{p1+0.2 vector,p1+0.4 vector}],Line[{p1+0.4 vector+orth width/4,p1+0.4 vector-orth width/4}],
Line[{p2-0.2 vector,p2-0.4 vector}],Line[{p2-0.4 vector+orth width/4,p2-0.4 vector-orth width/4}]
}
];


Options[DCBlock]={AspectRatio->1,"Width"->Automatic,Background->None};


SpectrumAnalyzerCircle[p1_List,p2_List,OptionsPattern[]]:=Module[{c,r,sr,f},
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
sr=0.8;
f=OptionValue["Frequency"];
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
FirstCase[Plot[r Sinc[f/r(\[FormalX]-c[[1]])]+c[[2]]-( r/2) r,{\[FormalX],c[[1]]-sr r,c[[1]]+sr  r},PlotRange->Full],Line[__],Missing[],\[Infinity]]}
];


Options[SpectrumAnalyzerCircle]={Background->None,"Frequency"->24};


SpectrumAnalyzerBlock[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts,c,r,sr,f},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
sr=0.8;
f=OptionValue["Frequency"];
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
FirstCase[Plot[(width/2) Sinc[f/r(\[FormalX]-c[[1]])]+c[[2]]-(sr/2) (width/2),{\[FormalX],c[[1]]-sr r,c[[1]]+sr  r},PlotRange->Full],Line[__],Missing[],\[Infinity]]
}
];


Options[SpectrumAnalyzerBlock]={AspectRatio->1,"Width"->Automatic,Background->None,"Frequency"->24};


Oscilloscope[p1_List,p2_List,OptionsPattern[]]:=Module[{c,r,sr},
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
sr=0.75;
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
Line[{c-{sr r,0},c+{0,sr r/2},c-{0,sr r/2},c+{sr r,0}}]
}
];


Options[Oscilloscope]={Background->None};


JosephsonJunction[p1_List,p2_List,OptionsPattern[]]:=Module[{center,halfvector},
center=(p1+p2)/2;
halfvector=(p2-p1)/2;
Sequence@@{
Line[{p1,p2}],
Line[{center-{{1/Sqrt[2],-(1/Sqrt[2])},{1/Sqrt[2],1/Sqrt[2]}} . halfvector,center+{{1/Sqrt[2],-(1/Sqrt[2])},{1/Sqrt[2],1/Sqrt[2]}} . halfvector}],
Line[{center-{{1/Sqrt[2],1/Sqrt[2]},{-(1/Sqrt[2]),1/Sqrt[2]}} . halfvector,center+{{1/Sqrt[2],1/Sqrt[2]},{-(1/Sqrt[2]),1/Sqrt[2]}} . halfvector}]
}
];


Options[JosephsonJunction]={};


Squid[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts,halfcross},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
halfcross=OptionValue["Ratio"] vector/2;
Sequence@@{
JoinedCurve[Line[pts],CurveClosed->True],
Line[{pts[[1]]+vector/2-{{1/Sqrt[2],-(1/Sqrt[2])},{1/Sqrt[2],1/Sqrt[2]}} . halfcross,pts[[1]]+vector/2+{{1/Sqrt[2],-(1/Sqrt[2])},{1/Sqrt[2],1/Sqrt[2]}} . halfcross}],
Line[{pts[[1]]+vector/2-{{1/Sqrt[2],1/Sqrt[2]},{-(1/Sqrt[2]),1/Sqrt[2]}} . halfcross,pts[[1]]+vector/2+{{1/Sqrt[2],1/Sqrt[2]},{-(1/Sqrt[2]),1/Sqrt[2]}} . halfcross}],
Line[{pts[[4]]+vector/2-{{1/Sqrt[2],-(1/Sqrt[2])},{1/Sqrt[2],1/Sqrt[2]}} . halfcross,pts[[4]]+vector/2+{{1/Sqrt[2],-(1/Sqrt[2])},{1/Sqrt[2],1/Sqrt[2]}} . halfcross}],
Line[{pts[[4]]+vector/2-{{1/Sqrt[2],1/Sqrt[2]},{-(1/Sqrt[2]),1/Sqrt[2]}} . halfcross,pts[[4]]+vector/2+{{1/Sqrt[2],1/Sqrt[2]},{-(1/Sqrt[2]),1/Sqrt[2]}} . halfcross}]

}
];


Options[Squid]={AspectRatio->1,"Width"->Automatic,"Ratio"->0.5};


NoiseDrive[p1_List,p2_List,OptionsPattern[]]:=Module[{c,r,sr,sh,n,f},
c=(p1+p2)/2;
r=Norm[p2-p1]/2;
sr=0.75;
sh=0.5;
n=OptionValue["Points"];
f=OptionValue["Frequency"];
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Disk[c,r]},Nothing],
Circle[c,r],
Line[Join[
{c+{-sr r,0}},
Table[c+{-sr r+\[FormalI] (2 sr r)/n,(2 Mod[\[FormalI],2]-1)r sh Sin[(\[FormalI]/n)f]},{\[FormalI],1,n-1}],
{c+{sr r,0}}
]]}
];


Options[NoiseDrive]={Background->None,"Points"->10,"Frequency"->6};


(* ::Subsection::Closed:: *)
(*Optics*)


Laser[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts,laserpt,laserr1,laserr2,laserr3,laserw,lasers},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
laserpt=p1+vector*11/16;
lasers=If[NumberQ@OptionValue["LaserSize"],OptionValue["LaserSize"],Min[width/2,length/4]];
laserr3=(3/4)*lasers;
laserr2=(2/4)*lasers;
laserr1=(1/4)*lasers;
laserw=If[NumberQ@OptionValue["LaserWidth"],OptionValue["LaserWidth"],lasers/30];
Sequence@@Join[{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Polygon[{p1+vector/8-laserw orth,p1+vector/8+laserw orth,laserpt+laserw orth,laserpt-laserw orth}],
Disk[laserpt,laserr1]
},
Table[Polygon[{laserpt+laserw{-Sin[\[Phi]],Cos[\[Phi]]},laserpt-laserw{-Sin[\[Phi]],Cos[\[Phi]]},laserpt+laserr2{Cos[\[Phi]],Sin[\[Phi]]}-laserw{-Sin[\[Phi]],Cos[\[Phi]]},laserpt+laserr2{Cos[\[Phi]],Sin[\[Phi]]}+laserw{-Sin[\[Phi]],Cos[\[Phi]]}}],{\[Phi],2\[Pi] Range[12]/12+\[Pi]/12}],
Table[Polygon[{laserpt+laserw{-Sin[\[Phi]],Cos[\[Phi]]},laserpt-laserw{-Sin[\[Phi]],Cos[\[Phi]]},laserpt+laserr3{Cos[\[Phi]],Sin[\[Phi]]}-laserw{-Sin[\[Phi]],Cos[\[Phi]]},laserpt+laserr3{Cos[\[Phi]],Sin[\[Phi]]}+laserw{-Sin[\[Phi]],Cos[\[Phi]]}}],{\[Phi],2\[Pi] Range[12]/12}]
]
];


Options[Laser]={AspectRatio->2,"Width"->Automatic,Background->None,"LaserWidth"->Automatic,"LaserSize"->Automatic};


MZM[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,x,y,margin,pts,center,vertexcolors,blend},
vector=p2-p1;
normvec=Normalize[vector];
orth=RotationMatrix[\[Pi]/2] . normvec;
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
y=width/(2 2);
x=length/4;
margin=If[OptionValue["Margin"]===Automatic,0.1 width,OptionValue["Margin"]];
blend=(y/Sin[\[Pi]/3])/(y/Sin[\[Pi]/3]+length/2-y Tan[\[Pi]/3]);

vertexcolors=If[OptionValue[VertexColors]=!=None,
{{#[[1]],#[[1]]},{#[[1]],#[[1]],#[[2]],#[[2]]},{#[[2]],#[[2]]}}&@
If[ListQ[OptionValue[VertexColors]],Switch[Length[OptionValue[VertexColors]],
2,OptionValue[VertexColors],
1,ConstantArray[First[OptionValue[VertexColors]],2]
],
ConstantArray[OptionValue[VertexColors],2]
],
ConstantArray[None,3]];

pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
center=Mean[pts];

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Line[{p1+normvec margin,p1+normvec x},VertexColors->vertexcolors[[1]]],
Line[{p1+normvec x,p1+normvec x+y/Tan[\[Pi]/3] normvec+y orth,p2-normvec x-y/Tan[\[Pi]/3] normvec+y orth,p2-normvec x},VertexColors->vertexcolors[[2]]],
Line[{p1+normvec x,p1+normvec x+y/Tan[\[Pi]/3] normvec-y orth,p2-normvec x-y/Tan[\[Pi]/3] normvec-y orth,p2-normvec x},VertexColors->vertexcolors[[2]]],
Line[{p2-normvec x,p2-normvec margin},VertexColors->vertexcolors[[3]]]
}
];


Options[MZM]={AspectRatio->1,"Width"->Automatic,"Margin"->Automatic,VertexColors->None,Background->None};


PolarizerBox[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,c,orth,length,width,pts,center,s,r,n,\[Theta]},
vector=p2-p1;
c=(p1+p2)/2;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
s=Min[length,width];
r=3s/8;
n=OptionValue["Lines"];
\[Theta]=OptionValue["PolarizerAngle"];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};

Sequence@@Join[{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Circle[c,r]
},
Table[Line[{c+RotationMatrix[\[Theta]] . {-Sqrt[r^2-y^2],y},c+RotationMatrix[\[Theta]] . {Sqrt[r^2-y^2],y}}],{y,-r+2 r Range[n]/(n+1)}]
]
];


Options[PolarizerBox]={AspectRatio->1,"Width"->Automatic,Background->None,"PolarizerAngle"->\[Pi]/2,"Lines"->5};


PhotoDetector[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,curve,s},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
s=(4/3)*(Sqrt[2]-1);
curve={
Line[{p1+orth width/2,p1+orth width/2+vector/2}],
BezierCurve[{p1+orth width/2+vector/2+s vector/2,p2+s orth width/2,p2}],
BezierCurve[{p2-s orth width/2,p1-orth width/2+vector/2+s vector/2,p1-orth width/2+vector/2}],
Line[{p1-orth width/2}]
};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],FilledCurve[curve]},Nothing],
JoinedCurve[curve,CurveClosed->True]
}
];


Options[PhotoDetector]={AspectRatio->1,"Width"->Automatic,Background->None};


CirculatorBox[p1_List,p2_List,dir_Integer:1,OptionsPattern[]]:=Module[{vector,c,orth,length,width,pts,textcurve,textc,r,\[Theta],l,\[Phi]arc,\[Phi]start,\[Phi]end,t},
vector=p2-p1;
c=(p1+p2)/2;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
r=Min[length,width]/2;
t=1/2;
l=r OptionValue["ArrowScaling"];
\[Theta]=OptionValue["ArrowAngle"];
\[Phi]arc=OptionValue["Arc"];
\[Phi]start=Switch[OptionValue["StartAngle"],
	Automatic,ArcTan@@(p1-p2),
	"Fixed",-\[Pi] +((dir-1)/2) \[Pi]/4,
	_?(NumberQ@N@#&),OptionValue["StartAngle"]
];
\[Phi]end=\[Phi]start+dir \[Phi]arc;
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Circle[c,r/2,{\[Phi]start,\[Phi]end}],
Line[{
c+{t r Cos[\[Phi]end],t r Sin[\[Phi]end]}+RotationMatrix[\[Phi]end-dir 2\[Theta]] . {0,-l dir},
c+{t r Cos[\[Phi]end],t r Sin[\[Phi]end]},
c+{t r Cos[\[Phi]end],t r Sin[\[Phi]end]}+RotationMatrix[\[Phi]end+dir \[Theta]] . {0,-l dir}
}]
}
];


Options[CirculatorBox]={Background->None,AspectRatio->1,"Width"->Automatic,"ArrowScaling"->0.25,"ArrowAngle"->\[Pi]/8,"StartAngle"->"Fixed","Arc"->6\[Pi]/4};


AmplifierBox[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,c,orth,length,width,pts,center,s,w,pts2},
vector=p2-p1;
normvec=Normalize[vector];
c=(p1+p2)/2;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
s=Min[length,width];
w=3s/4;
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
pts2={c+normvec w Sqrt[3]/4,c-normvec w Sqrt[3]/4+orth w/2,c-normvec w Sqrt[3]/4-orth w/2};

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
JoinedCurve[Line[pts2],CurveClosed->True]
}
];


Options[AmplifierBox]={AspectRatio->1,"Width"->Automatic,Background->None};


Stripper[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,orth,length,width,pts,w1,w2},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
w1=OptionValue["FiberWidth"];
w2=OptionValue["BareFiberWidth"];
pts={p1+orth width/2,p2+orth width/2,p2-orth width/2,p1-orth width/2};
Sequence@@Join[{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True]
},
Flatten[Table[{Line[{p1+orth (y+w1/2),p2+orth (y+w2/2)}],Line[{p1+orth (y-w1/2),p2+orth (y-w2/2)}]},{y,{-width/4,width/4}}],1]
]
];


Options[Stripper]={AspectRatio->2,"Width"->Automatic,"FiberWidth"->1/4,"BareFiberWidth"->1/8,Background->None};


VGroove[p1_List,p2_List,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,pts,v,y},
vector=p2-p1;
normvec=Normalize[vector];
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=If[NumberQ@OptionValue["Width"],OptionValue["Width"],length/OptionValue[AspectRatio]];
v=OptionValue["FiberWidth"];
y=width/4;
pts={
	p1+orth width/2,p2+orth width/2,
	p2+orth (y+v/2),p2+orth y-normvec v,p2+orth (y-v/2),
	p2-orth (y-v/2),p2-orth y-normvec v,p2-orth (y+v/2),
	p2-orth width/2,p1-orth width/2,
	p1-orth (y+v/2),p1-orth y+normvec v,p1-orth (y-v/2),
	p1+orth (y-v/2),p1+orth y+normvec v,p1+orth (y+v/2)
};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True]
}
];


Options[VGroove]={AspectRatio->1,"Width"->Automatic,"FiberWidth"->1/4,Background->None};


FiberConnector[p_List,\[Phi]_,OptionsPattern[]]:=Module[{cwidth,t},
cwidth=OptionValue["ConnectorWidth"];
t=If[NumberQ@OptionValue["Thickness"],OptionValue["Thickness"],cwidth/8];
Polygon[{p+{Tan[\[Phi]]cwidth/2,cwidth/2}-{t/2,0},p+{Tan[\[Phi]]cwidth/2,cwidth/2}+{t/2,0},p-{Tan[\[Phi]]cwidth/2,cwidth/2}+{t/2,0},p-{Tan[\[Phi]]cwidth/2,cwidth/2}-{t/2,0}}]
]


Options[FiberConnector]={"Thickness"->Automatic,"ConnectorWidth"->1};


FiberBox[p1_List,p2_List,\[Phi]_,OptionsPattern[]]:=Module[{vector,normvec,orth,angle,t,width,cwidth,pts},
vector=p2-p1;
normvec=Normalize[vector];
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
width=OptionValue["Width"];
cwidth=OptionValue["ConnectorWidth"];
t=If[NumberQ@OptionValue["Thickness"],OptionValue["Thickness"],cwidth/8];
pts={p1+orth width/2+normvec Tan[\[Phi]]width/2,p2+orth width/2+normvec Tan[\[Phi]]width/2,p2-orth width/2-normvec Tan[\[Phi]]width/2,p1-orth width/2-normvec Tan[\[Phi]]width/2};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True],
Polygon[{p1+orth cwidth/2+normvec (Tan[\[Phi]]cwidth/2-t/2),p1+orth cwidth/2+normvec (Tan[\[Phi]]cwidth/2+t/2),p1-orth cwidth/2-normvec (Tan[\[Phi]]cwidth/2-t/2),p1-orth cwidth/2-normvec (Tan[\[Phi]]cwidth/2+t/2)}],
Polygon[{p2+orth cwidth/2+normvec (Tan[\[Phi]]cwidth/2-t/2),p2+orth cwidth/2+normvec (Tan[\[Phi]]cwidth/2+t/2),p2-orth cwidth/2-normvec (Tan[\[Phi]]cwidth/2-t/2),p2-orth cwidth/2-normvec (Tan[\[Phi]]cwidth/2+t/2)}]
}
]


Options[FiberBox]={"Thickness"->Automatic,"Width"->1,"ConnectorWidth"->1,Background->None};


FiberLine[p1_List,p2_List,\[Phi]1_:0,\[Phi]2_:0,OptionsPattern[]]:=Module[{vector,normvec,orth,length,width,pts},
vector=p2-p1;
normvec=Normalize[vector];
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];
width=OptionValue["FiberWidth"];
pts={p1+orth width/2+normvec Tan[\[Phi]1]width/2,p2+orth width/2+normvec Tan[\[Phi]2]width/2,p2-orth width/2-normvec Tan[\[Phi]2]width/2,p1-orth width/2-normvec Tan[\[Phi]1]width/2};
Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],Polygon[pts]},Nothing],
JoinedCurve[Line[pts],CurveClosed->True]
}
];


Options[FiberLine]={"FiberWidth"->1/4,Background->None};


FiberHalfBend[p1_List,p2_List,\[Phi]1_:0,\[Phi]2_:0,dir_List:{1,0},OptionsPattern[]]:=Module[{vector,para,orth,paradist,orthdist,center,r,d,x1,x2,width,curve},
vector=p2-p1;
para=Normalize[dir];
orth=Normalize[vector];
paradist=para . vector;
orthdist=orth . vector;
If[paradist>0,Message[FiberHalfBend::dir,dir,{p1,p2}]];
center=(p1+p2)/2;
d=Det[{para,orth}];
r=orthdist/2;

width=OptionValue["FiberWidth"];
x1= Tan[\[Phi]1]width/2;
x2= Tan[\[Phi]2]width/2;

curve={
BezierCurve[MapAt[(#-d x1 para)&,QuarterCircleBezierPoints[center,r+width/2,ArcTan@@(-orth),d],1]],
BezierCurve[Rest@Reverse@MapAt[(#+d x2 para)&,QuarterCircleBezierPoints[center,r+width/2,ArcTan@@(orth),-d],1]],
Line[{p2-orth width/2-d x2 para}],
BezierCurve[Rest@MapAt[(#-d x2 para)&,QuarterCircleBezierPoints[center,r-width/2,ArcTan@@(orth),-d],1]],
BezierCurve[Rest@Reverse@MapAt[(#+d x1 para)&,QuarterCircleBezierPoints[center,r-width/2,ArcTan@@(-orth),d],1]]
};

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],FilledCurve[curve]},Nothing],
JoinedCurve[curve,CurveClosed->True]
}
];


Options[FiberHalfBend]={"FiberWidth"->1/4,Background->None};


FiberS[p1_List,p2_List,\[Phi]1_:0,\[Phi]2_:0,dir_List:{1,0},OptionsPattern[]]:=Module[{vector,para,orth,paradist,orthdist,d,r,\[Phi],x1,x2,width,curve},
vector=p2-p1;
para=Normalize[(dir . vector)dir];
orth=Normalize[(RotationMatrix[\[Pi]/2] . dir . vector)RotationMatrix[\[Pi]/2] . dir];
paradist=para . vector;
orthdist=orth . vector;
d=Det[{para,orth}];
r=(paradist^2+orthdist^2)/(4 orthdist);
\[Phi]=ArcTan[paradist^2-orthdist^2,2 paradist orthdist];

width=OptionValue["FiberWidth"];
x1=Tan[\[Phi]1]width/2;
x2=Tan[\[Phi]2]width/2;

curve={
BezierCurve[MapAt[(#-d x1 para)&,AcuteCircleBezierPoints[p1+r orth,r+width/2,ArcTan@@(-orth),d \[Phi]],1]],
BezierCurve[Rest@Reverse@MapAt[(#-d x2 para)&,AcuteCircleBezierPoints[p2-r orth,r-width/2,ArcTan@@(orth),d \[Phi]],1]],
Line[{p2+orth width/2+d x2 para}],
BezierCurve[Rest@MapAt[(#+d x2 para)&,AcuteCircleBezierPoints[p2-r orth,r+width/2,ArcTan@@(orth),d \[Phi]],1]],
BezierCurve[Rest@Reverse@MapAt[(#+d x1 para)&,AcuteCircleBezierPoints[p1+r orth,r-width/2,ArcTan@@(-orth),d \[Phi]],1]]
};

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],FilledCurve[curve]},Nothing],
JoinedCurve[curve,CurveClosed->True]
}
];


Options[FiberS]={"FiberWidth"->1/4,Background->None};


(* ::Subsection::Closed:: *)
(*Markers*)


CurrentArrow[p_List,dir_List,size_,OptionsPattern[]]:=Module[{ndir,orth,vertexcolors},
ndir=Normalize[dir];
orth=RotationMatrix[\[Pi]/2] . ndir;
Line[{
p-ndir size/2+orth size/2,
p+ndir size/2,
p-ndir size/2-orth size/2
}]
];


Options[CurrentArrow]={};


VoltageArrow[p_List,dir_Integer,size_,OptionsPattern[]]:=Module[{r,\[Theta],l,\[Phi]arc,\[Phi]start,\[Phi]end},
r=size/2;
l=r OptionValue["ArrowScaling"];
\[Theta]=OptionValue["ArrowAngle"];
\[Phi]arc=OptionValue["Arc"];
\[Phi]start=OptionValue["StartAngle"];
\[Phi]end=\[Phi]start+dir \[Phi]arc;
Sequence@@{
Circle[p,r,{\[Phi]start,\[Phi]end}],
Line[{
p+{r Cos[\[Phi]end],r Sin[\[Phi]end]}+RotationMatrix[\[Phi]end-dir 2\[Theta]] . {0,-l dir},
p+{r Cos[\[Phi]end],r Sin[\[Phi]end]},
p+{r Cos[\[Phi]end],r Sin[\[Phi]end]}+RotationMatrix[\[Phi]end+dir \[Theta]] . {0,-l dir}
}]}
];


Options[VoltageArrow]={"ArrowScaling"->0.5,"ArrowAngle"->\[Pi]/8,"StartAngle"->0,"Arc"->5\[Pi]/4};


(* ::Subsection::Closed:: *)
(*Compositions*)


ElbowAlgorithm[ptsA_List,ptsB_List,OptionsPattern[]]:=Module[{
pA,pB,dirA,dirB,maxelemsize,
dist,region,solve,soln,situation,point,dir3,solve2,idist,corners},
pA=ptsA[[2]];
pB=ptsB[[1]];
dirA=Normalize[Subtract@@(Reverse@ptsA)];
dirB=Normalize[Subtract@@(Reverse@ptsB)];
maxelemsize=Max[Norm/@{Subtract@@ptsA,Subtract@@ptsB}];

dist=Norm[pA-pB];
region=RegionIntersection[
ParametricRegion[pA+\[FormalS] dirA +\[FormalT] RotationMatrix[\[Pi]/2] . dirA,{{\[FormalS],0,2 dist},{\[FormalT],-2 dist,2 dist}}],
ParametricRegion[pB-\[FormalS] dirB +\[FormalT] RotationMatrix[\[Pi]/2] . dirB,{{\[FormalS],0,2 dist},{\[FormalT],-2 dist,2 dist}}]
];

solve=Solve[pA+\[FormalI] dirA==pB-\[FormalJ] dirB];

(* work out if we need additional corners *)
situation=If[Length[solve]>0,
soln=First[solve];
If[Length[DeleteCases[soln,(_->0.)|(_->0)]]>1,
point=pA+\[FormalI] dirA/.soln;
If[point\[Element]region,
1,
2],
0],
2
];

corners=Switch[situation,
0,{},
1,{point},
2,
dir3=If[Length[solve]>0,
RotationMatrix[\[Pi]/2] . Mean[{Sign[\[FormalI]]dirA,-Sign[\[FormalJ]]dirB}/.soln],
RotationMatrix[\[Pi]/2] . dirA
];
solve2=Solve[pA+\[FormalI] dirA+\[FormalK] dir3==pB-\[FormalI] dirB,{\[FormalI]}];
If[(Length[solve2]>0),
If[(\[FormalI]/.First[solve2])>0,
(* additional corners in between *)
{pA+\[FormalI] dirA,pB-\[FormalI] dirB}/.First[solve2],
(* facing opposite directions *)
idist=Switch[OptionValue["OvershootLength"],
Automatic,-\[FormalI]/.First[Solve[pA+\[FormalI] dirA+\[FormalK] dir3==pB-\[FormalI] dirB,{\[FormalI],\[FormalK]}]],
"ElementSize",maxelemsize,
_?NumberQ,OptionValue["OvershootLength"]
];
{pA+idist dirA,pB-idist dirB}
],
(* additional corners outside *)
idist=Switch[OptionValue["OvershootLength"],
Automatic,idist=\[FormalI]/.First[Solve[pA+\[FormalI] dirA+\[FormalK] dir3==pB+\[FormalI] dirB,{\[FormalI],\[FormalK]}]],
"ElementSize",maxelemsize,
_?NumberQ,OptionValue["OvershootLength"]
];
Which[
pA+idist dirA \[Element] region,
{pA+idist dirA,pB-\[FormalJ] dirB}/.First@Solve[pA+idist dirA+\[FormalK] dir3==pB-\[FormalJ] dirB,{\[FormalJ],\[FormalK]}],
pB-idist dirB \[Element] region,
{pA+\[FormalI] dirA,pB-idist dirB}/.First@Solve[pA+\[FormalI] dirA+\[FormalK] dir3==pB-idist dirB,{\[FormalI],\[FormalK]}],
True,
{pA+idist dirA,pB-idist dirB}
]
]
];

Line[Join[{pA},corners,{pB}]]

];


Options[ElbowAlgorithm]={"OvershootLength"->"ElementSize"};


Circuit[list_List,OptionsPattern[]]:=Module[{link,loc1,loc2,holdlist},
holdlist=ReleaseHold@Map[Hold,Hold[list],{2}];

Switch[OptionValue["Connections"],
	"Line",
	link[\[FormalA]_List,\[FormalB]_List]:=Line[{\[FormalA][[2]],\[FormalB][[1]]}],
	"Elbow",
	link[\[FormalA]_List,\[FormalB]_List]:=ElbowAlgorithm[\[FormalA],\[FormalB],"OvershootLength"->OptionValue["OvershootLength"]],
	_,
	Message[Circuit::connection,OptionValue["Connections"]];link[\[FormalA]_List,\[FormalB]_List]:=Nothing
];
link[Nothing,_]:=Nothing;

loc2=Nothing;
Map[
(loc1=loc2;
ReleaseHold[
#/.Join[
	(#[\[FormalX]_,\[FormalY]_,\[FormalZ]___]:>(loc2={\[FormalX],\[FormalY]};Sequence@@{link[loc1,loc2],#[\[FormalX],\[FormalY],\[FormalZ]]}))&/@$CircuitObjects,
	{
		Point[\[FormalX]_,\[FormalZ]___]:>(loc2={\[FormalX],\[FormalX]};Sequence@@{link[loc1,loc2],Point[\[FormalX],\[FormalZ]]}),
		Line[\[FormalX]_,\[FormalZ]___]:>(loc2=\[FormalX][[{1,-1}]];Sequence@@{link[loc1,loc2],Line[\[FormalX],\[FormalZ]]}),
		Disk[\[FormalX]_,\[FormalZ]___]:>(loc2={\[FormalX],\[FormalX]};Sequence@@{link[loc1,loc2],Disk[\[FormalX],\[FormalZ]]}),
		Circle[\[FormalX]_,\[FormalZ]___]:>(loc2={\[FormalX],\[FormalX]};Sequence@@{link[loc1,loc2],Circle[\[FormalX],\[FormalZ]]})
	}]
])&,holdlist]

];


Options[Circuit]={"Connections"->"Elbow","OvershootLength"->"ElementSize"};


SetAttributes[Circuit,HoldAll];


(* ::Subsection::Closed:: *)
(*Tools*)


$SubdivideLineOptions={"MinimumLength"->0,"MaximumSegments"->100};


SubdivideLineSegment[Line[{p1_,p2_},VertexColors->{col1_,col2_}],opts:OptionsPattern[$SubdivideLineOptions]]:=Module[{l=Norm[p2-p1],n,minl=OptionValue["MinimumLength"],maxn=OptionValue["MaximumSegments"]},
If[l<minl,Message[SubdivideLine::small];Line[{p1,p2},VertexColors->{col1,col2}],
n=If[minl>0,Max[maxn,Floor[l/minl]],maxn];
Table[Line[{p1+(\[FormalI]-1)/n (p2-p1),p1+\[FormalI]/n (p2-p1)},VertexColors->{Blend[{col1,col2},(\[FormalI]-1)/n],Blend[{col1,col2},\[FormalI]/n]}],{\[FormalI],n}]
]
];


SubdivideLine[Line[pts_,VertexColors->cols_List],opts:OptionsPattern[$SubdivideLineOptions]]:=
If[(OptionValue["MinimumLength"]==0)\[And](OptionValue["MaximumSegments"]==\[Infinity]),
Message[SubdivideLine::opts];Line[pts,VertexColors->cols],
Module[{sublines=Partition[pts,2,1],n,subcols},
n=Length[sublines];
subcols=If[Length[cols]==n+1,cols,Table[Blend[cols,\[FormalI]/n],{\[FormalI],0,n}]];
Join@@Table[SubdivideLineSegment[Line[sublines[[\[FormalI]]],VertexColors->subcols[[\[FormalI];;\[FormalI]+1]]],opts],{\[FormalI],n}]
]
];
SubdivideLine[Line[pts_],___]:=Line[pts];
SubdivideLine[Line[pts_,VertexColors->None],___]:=Line[pts];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];


(* ::Section::Closed:: *)
(*Scrap*)


(*FiberHalfBend[{p1_List,dir1_,\[Phi]1_},{p2_List,dir2_,\[Phi]2_},OptionsPattern[]]:=Module[{normvec1,normvec2,orth1,orth2,vector,orth,length,r,s,x1,x2,width,curve},
vector=p2-p1;
length=Norm[vector];
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];

(*r=length;s=4/3 (Sqrt[2]-1);*)
r=length/2;s=1;

normvec1=Which[ListQ[dir1],Normalize[dir1],NumberQ[dir1],{Cos[dir1],Sin[dir1]}];
normvec2=Which[ListQ[dir2],Normalize[dir2],NumberQ[dir2],{Cos[dir2],Sin[dir2]}];

(*orth1=RotationMatrix[\[Pi]/2].normvec1;
orth2=RotationMatrix[\[Pi]/2].normvec2;*)
orth1=RotationMatrix[\[Pi]/2] . orth;
orth2=RotationMatrix[-\[Pi]/2] . orth;

width=OptionValue["FiberWidth"];
x1= Tan[\[Phi]1]width/2;
x2= Tan[\[Phi]2]width/2;

curve={
BezierCurve[{p1+normvec1 x1+orth1 width/2,p1+normvec1 x1+orth1 width/2+normvec1 (r+width/2) s,p2-normvec2 x2+orth2 width/2-normvec2 (r+width/2) s,p2-normvec2 x2+orth2 width/2}],
Line[{p2+orth2 width/2+normvec2 x2,p2-orth2 width/2-normvec2 x2}],
BezierCurve[{p2-normvec2 x2-orth2 width/2-normvec2 (r-width/2) s,p1+normvec1 x1-orth1 width/2+normvec1 (r-width/2) s,p1+normvec1 x1-orth1 width/2}],
Line[{p1-orth1 width/2-normvec1 x1}]
};

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],FilledCurve[curve]},Nothing],
JoinedCurve[curve,CurveClosed->True]
}
];*)


(*FiberS[p1_List,p2_List,\[Phi]1_:0,\[Phi]2_:0,OptionsPattern[]]:=Module[{normvec1,normvec2,orth1,orth2,vector,orth,length,r,s,x1,x2,width,curve},
vector=p2-p1;
orth=Normalize[RotationMatrix[\[Pi]/2] . vector];
length=Norm[vector];

(*r=length;s=4/3 (Sqrt[2]-1);*)
r=length;s=1/2;
r=length/2;s=1;

normvec1={1,0};
normvec2={1,0};

orth1=RotationMatrix[\[Pi]/2] . normvec1;
orth2=RotationMatrix[\[Pi]/2] . normvec2;

width=OptionValue["FiberWidth"];
x1= Tan[\[Phi]1]width/2;
x2= Tan[\[Phi]2]width/2;

curve={
BezierCurve[{p1+normvec1 x1+orth1 width/2,p1+normvec1 x1+orth1 width/2+normvec1 (r+width/2) s,p2-normvec2 x2+orth2 width/2-normvec2 (r+width/2) s,p2-normvec2 x2+orth2 width/2}],
Line[{p2+orth2 width/2+normvec2 x2,p2-orth2 width/2-normvec2 x2}],
BezierCurve[{p2-normvec2 x2-orth2 width/2-normvec2 (r-width/2) s,p1+normvec1 x1-orth1 width/2+normvec1 (r-width/2) s,p1+normvec1 x1-orth1 width/2}],
Line[{p1-orth1 width/2-normvec1 x1}]
};

Sequence@@{
If[OptionValue[Background]=!=None,{OptionValue[Background],FilledCurve[curve]},Nothing],
JoinedCurve[curve,CurveClosed->True]
}
];*)
